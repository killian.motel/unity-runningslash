﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public PieceType type;
    private Obstacles currentObstacle;

    public void Spawn()
    {
        currentObstacle = LevelManager.Instance.GetObstacles(type, 0);
        currentObstacle.gameObject.SetActive(true);
        currentObstacle.transform.SetParent(transform, false);
    }
    public void DeSpawn()
    {
        currentObstacle.gameObject.SetActive(false);
    }

}
