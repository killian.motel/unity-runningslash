﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { set; get; }
    
    // Level Spawning

    // List of pieces
    public List<Obstacles> walls = new List<Obstacles>();
    public List<Obstacles> piercedWalls = new List<Obstacles>();
    public List<Obstacles> hooks = new List<Obstacles>();
    public List<Obstacles> sideWalls = new List<Obstacles>();
    public List<Obstacles> obstacles = new List<Obstacles>(); // All the obstacle in the pool

    public Obstacles GetObstacles(PieceType pt, int visualIndex)
    {
        Obstacles p = obstacles.Find(x => x.type == pt && x.visualIndex == visualIndex && !x.gameObject.activeSelf);

        if(p == null)
        {
            GameObject go = null;
            if (pt == PieceType.wall)
                go = walls[visualIndex].gameObject;
            else if (pt == PieceType.piercedWall)
                go = piercedWalls[visualIndex].gameObject;
            else if (pt == PieceType.hook)
                go = hooks[visualIndex].gameObject;
            else if (pt == PieceType.sideWall)
                go = sideWalls[visualIndex].gameObject;

            go = Instantiate(go);
            p = go.GetComponent<Obstacles>();
            obstacles.Add(p);
           
        }

        return p;
    }
}
