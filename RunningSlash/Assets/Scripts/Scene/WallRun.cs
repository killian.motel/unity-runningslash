﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRun : MonoBehaviour
{
    public string side = "";
    PlayerController player;
    private void OnTriggerEnter(Collider collider)
    {
        
        if (collider.tag == "Player")
        {
            player = collider.gameObject.GetComponent<PlayerController>();
            player.SetWallRunning(true);
            player.SetWallRunningSide(side);
            player.SetWallRunningPosition(transform.position);

        }
    }

    private void OnTriggerExit(Collider collider)
    {
        player.SetWallRunning(false);
        player.SetWallRunningSide("");
        player.SetSideSpeed(0f);
        player.SetIsWallRunning(false);
    }
}
