﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PieceType
{
    none = -1,
    wall = 0,
    piercedWall = 1,
    hook = 2,
    sideWall = 3,
}
public class Obstacles : MonoBehaviour
{
    public PieceType type;
    public int visualIndex;

}
