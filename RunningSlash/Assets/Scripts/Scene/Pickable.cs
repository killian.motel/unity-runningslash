﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            collider.gameObject.GetComponent<PlayerController>().AddSpeed();
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        transform.Rotate(new Vector3(0f, 0f, 1f));
    }
}
