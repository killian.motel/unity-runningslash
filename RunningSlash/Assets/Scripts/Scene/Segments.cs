﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segments : MonoBehaviour
{
    public int SegId { set; get; }
    public bool transition;

    public int lenght;
    public int beginY1;
    public int endY1;

    private Obstacles[] obstacles;

    private void Awake()
    {
        obstacles = gameObject.GetComponentsInChildren<Obstacles>();
    }
    public void Spawn()
    {
        gameObject.SetActive(true);
    }
    public void DeSpawn()
    {
        gameObject.SetActive(false);
    }
}
