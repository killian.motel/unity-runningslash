﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public Transform lookAt;

    private Vector3 cameraOffset = new Vector3(0 , 5.0f, -5.0f);
    private PlayerController player;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    private void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }
    // Update is called once per frame
    void LateUpdate()
    {
        if (!player.isRunning)
            return;

        Vector3 newPos = lookAt.position + cameraOffset;
        newPos.x = 0;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime);
    }
}
