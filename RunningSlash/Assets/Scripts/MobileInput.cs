using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInput : MonoBehaviour
{
    private const float DEADZONE = 100.0f;
    private float startTime;

    public static MobileInput Instance { set; get; }

    private bool tap, swipeLeft, swipeRight, swipeDown, swipeUp, longTouch;
    private Vector2 swipeDelta, startTouch;

    public bool Tap { get { return tap; } }
    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeLeft { get { return swipeLeft; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeDown { get { return swipeDown; } }
    public bool SwipeUp { get { return swipeUp; } }
    public bool LongTouch { get { return longTouch; } }

    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        tap = swipeLeft = swipeRight = swipeDown = swipeUp = longTouch = false;

        #region Standalone Inputs
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            startTouch = Input.mousePosition;
            startTime = Time.time;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            startTouch = swipeDelta = Vector2.zero;
            startTime = 0f;
        }
        #endregion

        #region Mobile Inputs
        if (Input.touches.Length != 0)
        {
            if(Input.touches[0].phase == TouchPhase.Began)
            {
                tap = true;
                startTouch = Input.mousePosition;
                startTime = Time.time;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                startTouch = swipeDelta = Vector2.zero;
                startTime = 0f;
            }

        }

        #endregion


        // Calculate distance
        if(startTouch != Vector2.zero)
        {
            if (Input.touches.Length != 0) // Mobile
            {
                if (Time.time - startTime > 0.3f)
                {
                    longTouch = true;
                }
                swipeDelta = Input.touches[0].position - startTouch;
            }
            else if (Input.GetMouseButton(0)) // Standalone
            {
                if (Time.time - startTime > 0.3f)
                {
                    longTouch = true;
                }
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        // Swipes
        if(swipeDelta.magnitude > DEADZONE)
        {
            float x = swipeDelta.x;
            float y = swipeDelta.y;

            if(Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                    swipeLeft = true;
                else
                    swipeRight = true;
            }
            else
            {
                if (y < 0)
                    swipeDown = true;
                else
                    swipeUp = true;
            }

            startTouch = swipeDelta = Vector2.zero;
        }
    }
}
