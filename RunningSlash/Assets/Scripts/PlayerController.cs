﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public bool isRunning = false;

    private bool crashed = false;

    private CharacterController myBody;
    public ParticleSystem leftParticle;
    public ParticleSystem rightParticle;
    public ParticleSystem rightWallParticle;
    public ParticleSystem leftWallParticle;
    public ParticleSystem hookParticle;
    public ParticleSystem deathExplosion;

    private Touch touch;

    public TextMeshProUGUI speedText;
    public TextMeshProUGUI kmh;
    public Vector3 speed;
    public float actualSpeed = 35f;
    public float forwardSpeed = 0f;
    public float sideSpeed = 0f;
    public float maxSideSpeed = 10f;
    public float jumpForce = 20f;
    public float verticalVelocity = 0f;
    public float increasingSpeed = 1f;
    public float decreasingSpeed = 0.2f;
    private float speedTime;

    public float gravity = 40f;
    public float wallGravity = 10f;
    public bool canWallRun = false;
    public bool isWallRunning = false;
    public Vector3 wallRunningPosition;
    public string wallRunningSide = "";
    public float wallRunGravity;

    public bool canHook = false;
    public bool isHooking = false;
    public Vector3 hookSpot = new Vector3(0f, 0f, 0f);
    public float yCurve = 0f;

    public AudioSource bgMusic;
    protected Animator myAnimator;

    //Hook
    public Transform hook;
    public Vector3 p0, p1, p2, middle, sym;
    // Using tParam with tSpeed for following the curve in a certain time
    public float tParam = 0f;
    public float tSpeed = 0.3f;
    public bool paramSets = false;
    public float distance = 0f;
    private float startTime;
    private int index = 0;
    private float timeBtw = 0.7f;
    private float acceleration = 0.9f;

    public LineRenderer LR;

    private void Awake()
    {
        speed = new Vector3(0f, 0f, forwardSpeed);
        crashed = false;
    }

    void Start()
    {
        myBody = GetComponent<CharacterController>();
        sideSpeed = 0f;
        rightWallParticle.Stop();
        leftWallParticle.Stop();
        hookParticle.Stop();
        leftParticle.Stop();
        rightParticle.Stop();
        deathExplosion.Stop();
        startTime = Time.time;
        timeBtw = 2f;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision : " + collision.collider.tag);
        if(collision.collider.tag == "Obstacles")
        {
            KillPlayer();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isRunning) 
            return;

        if (crashed)
            KillPlayer();

        ControlMovementWithKeyboard();
        if (!isHooking)
            MovePlayer();

        if(verticalVelocity < -70f) { KillPlayer(); }

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                transform.position = new Vector3(
                    transform.position.x + touch.deltaPosition.x * sideSpeed,
                    transform.position.y + 100,
                    transform.position.z + 100);
            }
        }

        if (canHook)
        {
            if (MobileInput.Instance.LongTouch) // Press the screen for more than 0.3s
            {
                if (!paramSets) // Do that only once
                {
                    verticalVelocity = 5f;
                    hookParticle.Play();

                    p0 = transform.position; // Take as p0 the player position
                    p1 = hookSpot; // Take as p1 the attach spot (white cube in the video)

                    // Calculate the hook size
                    distance = Mathf.Sqrt(Mathf.Pow(p0.x - p2.x, 2) + Mathf.Pow(p0.y - p1.y, 2) + Mathf.Pow(p0.z - p1.z, 2));

                    // The arrive point
                    p2 = new Vector3(-p0.x, p1.y, p1.z + distance);

                    middle = Vector3.Lerp(p0, p2, 0.5f);
                    sym = new Vector3(p1.x + (2 * (middle.x - p1.x)), p1.y + (2 * (middle.y - p1.y)) - 5f, p1.z + (2 * (middle.z - p1.z)));

                    Debug.DrawLine(p1, p0, Color.black, 5f);
                    Debug.DrawLine(p1, p2, Color.black, 5f);

                    paramSets = true;
                }

                Grapple();
            }
            else
            {  // If stop touching
                isHooking = false;
                LR.enabled = false;
            }

        }
        else
        {
            index = 0;
            paramSets = false;
        }
    

        if (canWallRun)
        {
            if (wallRunningSide == "right")
            {
                if (MobileInput.Instance.SwipeRight)
                {
                    isWallRunning = true;
                    WallRun();
                    rightWallParticle.Play();
                }
            }
            else if (wallRunningSide == "left")
            {
                if (MobileInput.Instance.SwipeLeft)
                {
                    isWallRunning = true;
                    WallRun();
                    leftWallParticle.Play();
                }
            }
            if (IsGrounded()) sideSpeed = 0f;
        }
        else
        {
            if (rightWallParticle.isPlaying) rightWallParticle.Stop();
            if (leftWallParticle.isPlaying) leftWallParticle.Stop();
        }

        if (Time.time - speedTime > 0.1f)
        {
            speedTime = Time.time;
            actualSpeed -= decreasingSpeed;
            if (actualSpeed < 0) { actualSpeed = 0; }

            speedText.text = forwardSpeed.ToString("F0");
            if (forwardSpeed <= 20) { KillPlayer(); }
            else if (forwardSpeed <= 25) { speedText.faceColor = new Color(233, 35, 36, 255); }
            else if (forwardSpeed >= 35) { speedText.faceColor = new Color(128, 204, 32, 255); }
            else { speedText.faceColor = new Color(204, 172, 32, 255); }

           
        }
    }

    protected void MoveForward()
    {
        if (!isHooking)
        {
            var normValue = Mathf.InverseLerp(0, 100, actualSpeed);
            forwardSpeed = Mathf.Lerp(20, 40, normValue);
            speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);
        }
    }

    void MovePlayer()
    {

        myBody.Move(speed * Time.deltaTime);
        if (IsGrounded() || isHooking)
        {
            if (!leftParticle.isPlaying) leftParticle.Play();
            if (!rightParticle.isPlaying) rightParticle.Play();

            verticalVelocity = -0.1f;
            if (MobileInput.Instance.SwipeUp)
            {
                verticalVelocity = jumpForce;
            }
        }
        else
        {
            if (leftParticle.isPlaying) leftParticle.Stop();
            if (rightParticle.isPlaying) rightParticle.Stop();

            if (isWallRunning)
                verticalVelocity -= (wallGravity * Time.deltaTime);
            else
                verticalVelocity -= (gravity * Time.deltaTime);

            if (MobileInput.Instance.SwipeDown)
            {
                verticalVelocity = -jumpForce;
            }
        }
        MoveForward();
    }

    protected bool IsGrounded()
    {
        Ray groundRay = new Ray(new Vector3(myBody.bounds.center.x,
            (myBody.bounds.center.y - myBody.bounds.extents.y) + 0.2f,
            myBody.bounds.center.z),
            Vector3.down);

        return (Physics.Raycast(groundRay, 0.4f));

    }

    private void ControlMovementWithKeyboard()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.Q))
        {
            sideSpeed = -maxSideSpeed;
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            sideSpeed = maxSideSpeed;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D))
        {
            sideSpeed = 0;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.Q))
        {
            sideSpeed = 0;
        }
    }

    private void WallRun()
    {
        if (wallRunningSide == "right") sideSpeed = 30f;
        else sideSpeed = -30f;
    }

    public void Grapple()
    {

        isHooking = true;

        LR.enabled = true;
        LR.SetPosition(0, hook.position);
        LR.SetPosition(1, hookSpot);

        var pointList = new List<Vector3>();
        for (float ratio = 0; ratio <= 1; ratio += 0.01f)
        {
            var tangentLineVertex1 = Vector3.Lerp(p0, sym, ratio);
            var bezierpoint = (Mathf.Pow(1 - ratio, 2f) * tangentLineVertex1 + 2 * (1 - ratio) * ratio * sym + Mathf.Pow(ratio, 2f) * p2);
            pointList.Add(bezierpoint);


            Debug.DrawLine(bezierpoint, new Vector3(bezierpoint.x, bezierpoint.y - 1f, bezierpoint.z), Color.white, 10f);
        }
        if (Time.time - startTime > timeBtw)
        {
            if (index < pointList.ToArray().Length)
            {
                transform.position = pointList[index];
                index++;
                timeBtw *= acceleration;
            }
        }

    }

    private void KillPlayer()
    {
        if (leftParticle.isPlaying) leftParticle.Stop();
        if (rightParticle.isPlaying) rightParticle.Stop();

        speedText.text = "";
        kmh.text = "";
        GetComponent<MeshRenderer>().enabled = false;
        isRunning = false;
        deathExplosion.Play();
        GameManager.Instance.OnDeath();

        isRunning = false;
    }

    public void SetWallRunning(bool canWallRun)
    {
        this.canWallRun = canWallRun;
    }
    public void SetIsWallRunning(bool isWallRunning)
    {
        this.isWallRunning = isWallRunning;
    }
    public void SetWallRunningPosition(Vector3 wallRunningPosition)
    {
        this.wallRunningPosition = wallRunningPosition;
    }
    public void SetWallRunningSide(string wallRunningSide)
    {
        this.wallRunningSide = wallRunningSide;
    }
    public void SetSideSpeed(float sideSpeed)
    {
        this.sideSpeed = sideSpeed;
    }
    public void SetIsHooking(bool isHooking)
    {
        this.isHooking = isHooking;
    }
    public void SetCanHook(bool canHook)
    {
        this.canHook = canHook;
    }
    public void SetHook(bool canHook)
    {
        this.canHook = canHook;
    }
    public void SetHookSpot(Vector3 spot)
    {
        hookSpot = spot;
    }
    public float GetSpeed()
    {
        return forwardSpeed;
    }
    public void SetCrash(bool crashed)
    {
        this.crashed = crashed;
    }

    public void AddSpeed()
    {
        GameManager.Instance.score += 1;
        actualSpeed += increasingSpeed;
        if (actualSpeed > 100) { actualSpeed = 100f; }
    }
    public void StartRunning()
    {
        isRunning = true;
    }

}
