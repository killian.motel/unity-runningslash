﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { set; get; }
    private static bool IsDead {set; get; }

    private float scoreTime;
    public int score = 0;
    private int highestScore = 0;

    private bool isGameStarted = false;
   

    private PlayerController player;
    public Animator deathMenuAnim;

    public Text actualScore;
    public Text deathScoreText;
    public Text actualHighestScore;
    public Text highestScoreText;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        actualScore.text = score.ToString();
    }
  
    // Update is called once per frame
    void Update()
    {
        if(MobileInput.Instance.Tap && !isGameStarted)
        {
            isGameStarted = true;
            player.StartRunning();
        }

        if (!isGameStarted || IsDead)
        {
            highestScoreText.text = "Highest score : ";
            actualHighestScore.text = highestScore.ToString();
            return;
        }
        
        if(highestScoreText.text == "Highest score : ")
        {
            highestScoreText.text = "";
            actualHighestScore.text = "";
        }

        if (Time.time - scoreTime > 0.5f)
        {
            scoreTime = Time.time;
            score++;
            actualScore.text = score.ToString() + " Pts";
        }
    } 

    public void OnPlayButton()
    {
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
    public void OnDeath()
    {
        IsDead = true;
        deathMenuAnim.SetTrigger("Dead");
        deathScoreText.text = score.ToString();
        
        if(score > highestScore) { highestScore = score; }

        highestScoreText.text = highestScore.ToString();
    }

    
}
