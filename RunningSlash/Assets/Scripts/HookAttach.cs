﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookAttach : MonoBehaviour
{

    PlayerController player;

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            player = collider.gameObject.GetComponent<PlayerController>();
            player.SetHook(true);
            player.SetCanHook(true);
            player.SetHookSpot(transform.position);
        }
    }
    private void OnTriggerExit(Collider collider)
    {
        player.SetCanHook(false);
        player.SetHook(false);
    }
}
